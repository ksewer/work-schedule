package sample.Controllers;

import com.jfoenix.controls.JFXButton;
import javafx.fxml.FXML;
import javafx.stage.Stage;

public class MenuEditDeleteController {

    @FXML
    private JFXButton btn_edit;
    @FXML
    private JFXButton btn_delete;

    private static Stage stage = null;

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    public void showEdit() {
        System.out.println("edit");
        GrafikController.closeStage();
    }

    public void askAboutDelete() {
        System.out.println("delete");
        GrafikController.closeStage();
    }
}
