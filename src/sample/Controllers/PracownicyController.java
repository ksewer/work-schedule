package sample.Controllers;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDrawer;
import com.jfoenix.controls.JFXHamburger;
import com.jfoenix.transitions.hamburger.HamburgerSlideCloseTransition;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import sample.AlertMaker;
import sample.Database.OperacjeDBPracownik;
import sample.Database.Pracownik;
import sample.Database.PracownikFX;

import java.io.IOException;
import java.net.URL;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;

public class PracownicyController implements Initializable {

    @FXML
    private JFXDrawer drawer;
    @FXML
    private StackPane rootPane;
    @FXML
    private AnchorPane rootAnchorPane;
    @FXML
    private JFXHamburger hamburger;
    @FXML
    private TableView<PracownikFX> workers_tableView;
    @FXML
    private TableColumn <PracownikFX,String> id_column;
    @FXML
    private TableColumn <PracownikFX,String> name_column;
    @FXML
    private TableColumn <PracownikFX,String> surname_column;
    @FXML
    private TableColumn <PracownikFX,String> login_column;
    @FXML
    private TableColumn <PracownikFX,String> position_column;
    @FXML
    private JFXButton btn_addWorker;
    @FXML
    private JFXButton btn_editWorker;
    @FXML
    private JFXButton btn_deleteWorker;
    @FXML
    private VBox vbox_menu;
    @FXML
    private VBox vbox_action;


    private static AddUserController addUserController;

    public static void setAddUserController(AddUserController addUserController) {
        PracownicyController.addUserController = addUserController;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        initDrawer();
        loadWorker();
    }

    private void initDrawer() {
        VBox toolbar = null;
        try {
            toolbar = FXMLLoader.load(getClass().getResource("/sample/toolbar/toolbar.fxml"));
            drawer.setSidePane(toolbar);
            drawer.setDefaultDrawerSize(200);
        } catch (IOException e) {
            e.printStackTrace();
        }

        HamburgerSlideCloseTransition task = new HamburgerSlideCloseTransition(hamburger);
        task.setRate(task.getRate() * -1);
        task.play();
        hamburger.addEventHandler(MouseEvent.MOUSE_CLICKED, (EventHandler<Event>) event -> {
            if(drawer.isClosed()){
                drawer.open();
                vbox_action.setMouseTransparent(true);
                btn_addWorker.setVisible(false);
                btn_editWorker.setVisible(false);
                btn_deleteWorker.setVisible(false);
            }else{
                drawer.close();
                vbox_action.setMouseTransparent(false);
                btn_addWorker.setVisible(true);
                btn_editWorker.setVisible(true);
                btn_deleteWorker.setVisible(true);
            }
        });
    }

    public void showAddWorker(){

        URL url = getClass().getClassLoader().getResource("sample/Layouts/AddUser.fxml");
        FXMLLoader loader = new FXMLLoader(url);
        Scene scene = null;
        try {
            scene = new Scene(loader.load());
        } catch (IOException e) {
            e.printStackTrace();
        }

        Stage primaryStage = (Stage) btn_addWorker.getScene().getWindow();
        Stage secondaryStage = new Stage();
        secondaryStage.setTitle("Chochołowy Dwór");
        secondaryStage.setScene(scene);


        secondaryStage.initModality(Modality.WINDOW_MODAL);
        secondaryStage.initOwner(primaryStage);
        AddUserController.setController(this);
        secondaryStage.show();

    }

    public void loadWorker(){

        List<Pracownik> workers = OperacjeDBPracownik.getPracownicy();

//        Pracownik pracownik = OperacjeDBPracownik.getPracownik(1);

        ObservableList<PracownikFX> data = FXCollections.observableArrayList();
        for(Pracownik p : workers){
            data.add(new PracownikFX(Integer.toString(p.getId()), p.getImie(), p.getNazwisko(), p.getStanowisko(), p.getLogin()));
        }
        id_column.setCellValueFactory(new PropertyValueFactory<>("id"));
        name_column.setCellValueFactory(new PropertyValueFactory<>("imie"));
        surname_column.setCellValueFactory(new PropertyValueFactory<>("nazwisko"));
        login_column.setCellValueFactory(new PropertyValueFactory<>("login"));
        position_column.setCellValueFactory(new PropertyValueFactory<>("stanowisko"));

        workers_tableView.setItems(data);
    }

    public void deleteWorker(){
        System.out.println("id list: " + workers_tableView.getSelectionModel().getFocusedIndex());
        System.out.println("id db: " + workers_tableView.getSelectionModel().getSelectedItem().getId());
        showDeleteDialog(workers_tableView.getSelectionModel().getSelectedItem());

    }

    private void showDeleteDialog(PracownikFX p){
        JFXButton yes_btn = new JFXButton("Tak");
        yes_btn.addEventHandler(MouseEvent.MOUSE_CLICKED,(MouseEvent e)->{
            System.out.println("Pracownik zostal usuniety!");
            OperacjeDBPracownik.usunPracownika(Integer.parseInt(p.getId()));
            loadWorker();
        });
        JFXButton no_btn = new JFXButton("Nie");
        AlertMaker.showMaterialDialog(rootPane,rootAnchorPane, Arrays.asList(yes_btn,no_btn),"Usuwanie pracownika","Czy chcesz usunąć pracownika "+p.getImie()+" "+p.getNazwisko()+"?");
    }

    public void editWorker(){
        int idWorker=-1;
        try {
            idWorker = Integer.parseInt(workers_tableView.getSelectionModel().getSelectedItem().getId());
            showAddWorker();

        }catch (Exception e){
            System.out.println("zaznacz pracownika");

            JFXButton ok_btn = new JFXButton("Ok");
            AlertMaker.showMaterialDialog(rootPane,rootAnchorPane, Arrays.asList(ok_btn),"Edycja pracownika","Jeżeli chcesz edytować pracownika, najpierw zaznacz go na liście pracowników!");
        }

        addUserController.setDataToEdit(idWorker);
    }

}
