package sample.Controllers;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import sample.AlertMaker;
import sample.Database.OperacjeDBPracownik;
import sample.Database.Pracownik;
import sample.Database.PracownikFX;

import java.net.URL;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;

public class AddUserController implements Initializable {


    @FXML
    private JFXButton confirm_btn;
    @FXML
    private StackPane rootPane;
    @FXML
    private AnchorPane rootAnchorPane;
    @FXML
    private JFXTextField au_name;
    @FXML
    private JFXTextField au_surname;
    @FXML
    private JFXTextField au_login;
    @FXML
    private JFXPasswordField au_password;
    @FXML
    private JFXTextField au_position;

    private static PracownicyController controller;
    private static boolean editWorker=false;
    private Pracownik editedWorker;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        au_name.setText("");
        au_surname.setText("");
        au_login.setText("");
        au_login.setDisable(false);
        au_position.setText("");
        au_password.setText("");
        PracownicyController.setAddUserController(this);
        confirm_btn.setText("Dodaj pracownika");
    }

    public void setDataToEdit(int idWorker){
        editWorker=true;
        editedWorker=OperacjeDBPracownik.getPracownik(idWorker);

            au_name.setText(editedWorker.getImie());
            au_surname.setText(editedWorker.getNazwisko());
            au_login.setText(editedWorker.getLogin());
            au_login.setDisable(true);
            au_position.setText(editedWorker.getStanowisko());
            au_password.setText(editedWorker.getHaslo());
            confirm_btn.setText("Edytuj pracownika");
    }


    public static void setController(PracownicyController controller) {
        AddUserController.controller = controller;
    }

    public void addWorker(){
        JFXButton ok_btn = new JFXButton("Ok");
        List<Pracownik> pracownicy = OperacjeDBPracownik.getPracownicy();
        if(editWorker==false) {



            boolean correctData = true;

            if(au_name.getText().trim().equals("")||au_surname.getText().trim().equals("")||au_login.getText().trim().equals("")||au_password.getText().trim().equals("")||au_position.getText().trim().equals("")){
                correctData=false;
                AlertMaker.showMaterialDialog(rootPane, rootAnchorPane, Arrays.asList(ok_btn), "Błąd dodawania", "Aby dodać pracownika musisz wypełnić wszystkie pola.");
            }else{
                for (Pracownik p : pracownicy) {
                    if (p.getLogin().equals(au_login.getText())) {
                        AlertMaker.showMaterialDialog(rootPane, rootAnchorPane, Arrays.asList(ok_btn), "Błąd dodawania", "Pracownik o loginie: '" + au_login.getText() + "' już istnieje! Wybierz inny login dla dodawanego pracownika.");
                        correctData = false;
                    }
                }
            }


            if (correctData) {
                OperacjeDBPracownik.dodajPracownika(au_name.getText(), au_surname.getText(), au_position.getText(), au_login.getText(), au_password.getText());


                JFXButton yes_btn = new JFXButton("Ok");
                yes_btn.addEventHandler(MouseEvent.MOUSE_CLICKED, (MouseEvent e) -> {
                    au_name.setText("");
                    au_surname.setText("");
                    au_login.setText("");
                    au_password.setText("");
                    au_position.setText("");
                });
                AlertMaker.showMaterialDialog(rootPane, rootAnchorPane, Arrays.asList(yes_btn), "Dodawanie nowego pracownika", "Pracownik " + au_name.getText() + " " + au_surname.getText() + " został dodany!");
            }
        }else{
            OperacjeDBPracownik.edytujPracownika(editedWorker.getId() ,au_name.getText(), au_surname.getText(), au_position.getText(), au_login.getText(), au_password.getText());
            AlertMaker.showMaterialDialog(rootPane, rootAnchorPane, Arrays.asList(ok_btn), "Edycja pracownika", "Pracownik " + au_name.getText() + " " + au_surname.getText() + " został edytowany!");
            Stage stage = (Stage) au_name.getScene().getWindow();
            stage.close();
        }

        controller.loadWorker();

    }


}
