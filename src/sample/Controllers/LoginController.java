package sample.Controllers;

import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;
import sample.Database.OperacjeDBPracownik;
import sample.Database.Pracownik;


import java.io.IOException;
import java.net.URL;


public class LoginController {

    @FXML
    JFXTextField login_tv;
    @FXML
    JFXPasswordField haslo_tv;



    public void keyPressed(KeyEvent e)
    {
        if (e.getCode() == KeyCode.ENTER)
        {
            zaloguj_btn();
        }
    }

    public void zaloguj_btn(){
        OperacjeDBPracownik operacjeDBPracownik = new OperacjeDBPracownik();
        try{
            Pracownik pracownik = operacjeDBPracownik.getPracownik(login_tv.getText());
            if(pracownik.getHaslo().equals(haslo_tv.getText())){
                System.out.println("Udalo Ci sie zalogowac! Zalogowano jako: "+pracownik.getImie()+" "+pracownik.getNazwisko());
                try {
                    URL url = getClass().getClassLoader().getResource("sample/Layouts/Scheduler.fxml");
                    FXMLLoader loader = new FXMLLoader(url);
                    Stage stage = (Stage) login_tv.getScene().getWindow();
                    stage.setX(250);
                    stage.setY(150);
                    Scene scene = new Scene(loader.load());
                    stage.setScene(scene);
                }catch (IOException io){
                    io.printStackTrace();
                }
            }else{
                System.out.println("Popraw sie!");
                login_tv.getStyleClass().add("wrong-data-login");
                haslo_tv.getStyleClass().add("wrong-data-login");
            }
        }catch (Exception e){
            login_tv.getStyleClass().add("wrong-data-login");
            haslo_tv.getStyleClass().add("wrong-data-login");
        }

    }
}
