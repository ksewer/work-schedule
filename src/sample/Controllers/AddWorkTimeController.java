package sample.Controllers;

import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXTextField;
import com.jfoenix.controls.JFXTimePicker;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.stage.Stage;
import sample.Database.OperationsDBTime;
import sample.Utilities.ParseDateStringToLocalDate;
import java.net.URL;
import java.time.Duration;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ResourceBundle;

public class AddWorkTimeController implements Initializable {

    @FXML
    JFXTextField worker_name;
    @FXML
    JFXTimePicker time_picker_from;
    @FXML
    JFXTimePicker time_picker_to;
    @FXML
    private JFXDatePicker date_work;
    @FXML
    private JFXTextField function_tf;

    private static String worker_name_str;
    private static int worker_id;
    private static GrafikController controller;

    public static void setController(GrafikController controller) {
        AddWorkTimeController.controller = controller;
    }

    public static void setWorker_name_str(String worker_name_str) {
        AddWorkTimeController.worker_name_str = worker_name_str;
    }

    public void set_time_picker_from(LocalTime timeFrom){
        time_picker_from.setValue(timeFrom);
    }

    public void set_time_picker_to(LocalTime timeTo){
        time_picker_to.setValue(timeTo);
    }

    public static void setWorker_id(int worker_id) {
        AddWorkTimeController.worker_id = worker_id;
    }

    public void addTime(){
        LocalTime date_from=time_picker_from.getValue();
        LocalTime date_to=time_picker_to.getValue();
//        System.out.println(date_from.compareTo(date_to));
        int diffMinutes = (int)Duration.between(date_from,date_to).toMinutes();
//        date_work.getValue().toString();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        controller.workTimesForDate.add(OperationsDBTime.addWorkTime(date_work.getValue().format(formatter).toString(),time_picker_from.getValue().toString(),time_picker_to.getValue().toString(),function_tf.getText(),diffMinutes,worker_id));
        String dateSplitted[]=date_work.getValue().format(formatter).toString().split("-");
        controller.createTable(Integer.parseInt(dateSplitted[1])-1,Integer.parseInt(dateSplitted[2]));
//        controller.searchElementToComplete();
        Stage stage = (Stage) worker_name.getScene().getWindow();
        stage.close();
    }

    public void setDate(String day, String month, String year){
        date_work.setValue(ParseDateStringToLocalDate.parse_date(day+"-"+month+"-"+year));
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        time_picker_from.set24HourView(true);
        time_picker_to.set24HourView(true);
        worker_name.setText(worker_name_str);
    }
}
