package sample.Controllers;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDrawer;
import com.jfoenix.controls.JFXHamburger;
import com.jfoenix.transitions.hamburger.HamburgerSlideCloseTransition;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import sample.Database.*;
import sample.Utilities.ParseMonthPolishStringToNumber;
import sample.Utilities.ParseMonthToPolishString;

import java.io.IOException;
import java.net.URL;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.ResourceBundle;

public class GrafikController extends Application implements Initializable {

    @FXML
    private JFXDrawer drawer;

    @FXML
    private JFXHamburger hamburger;

    @FXML
    private TableView<PracownikFX> scheduler_tableView;

    @FXML
    private TableColumn<PracownikFX, String> workers_column;

    @FXML
    private VBox vbox_action;

    @FXML
    private JFXButton btn_prevMonth;

    @FXML
    private Label monthLabel;

    @FXML
    private Label yearLabel;

    @FXML
    private JFXButton btn_nextMonth;


    ArrayList<WorkTime> workTimesForDate = new ArrayList<>();

    private int countColumn;

    private static Stage menuEditDeleteStage;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        initDrawer();
        showDate();
    }

    private void initDrawer() {
        VBox toolbar = null;
        try {
            toolbar = FXMLLoader.load(getClass().getResource("/sample/toolbar/toolbar.fxml"));
            drawer.setSidePane(toolbar);
            drawer.setDefaultDrawerSize(200);
        } catch (IOException e) {
            e.printStackTrace();
        }

        HamburgerSlideCloseTransition task = new HamburgerSlideCloseTransition(hamburger);
        task.setRate(task.getRate() * -1);
        task.play();
        hamburger.addEventHandler(MouseEvent.MOUSE_CLICKED, (EventHandler<Event>) event -> {
            if (drawer.isClosed()) {
                drawer.open();
                vbox_action.setMouseTransparent(true);
                btn_nextMonth.setVisible(false);
                btn_prevMonth.setVisible(false);
                monthLabel.setVisible(false);
                yearLabel.setVisible(false);
            } else {
                drawer.close();
                vbox_action.setMouseTransparent(false);
                btn_nextMonth.setVisible(true);
                btn_prevMonth.setVisible(true);
                monthLabel.setVisible(true);
                yearLabel.setVisible(true);
            }
        });
    }


    public void createTable(int month, int year) {
        scheduler_tableView.getColumns().clear();
//        cells.clear();
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.MONTH, month);
        calendar.set(Calendar.YEAR, year);
        countColumn = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
        TableColumn workers_column = new TableColumn("Pracownicy");
        List<Pracownik> workers = OperacjeDBPracownik.getPracownicy();
        ObservableList<PracownikFX> data = FXCollections.observableArrayList();

        for (Pracownik p : workers) {
            data.add(new PracownikFX(Integer.toString(p.getId()), p.getImie(), p.getNazwisko(), p.getStanowisko(), p.getLogin()));
        }
        workers_column.setCellValueFactory(new PropertyValueFactory<>("imie_nazwisko"));
        scheduler_tableView.getColumns().add(workers_column);


        int monthToCompleteTheTable = month + 1;
        String prefix = "";
        prefix = ParseMonthToPolishString.changeMonthToString(month).substring(0, 3);
        completeTheTable((monthToCompleteTheTable < 10 ? "0" + monthToCompleteTheTable : monthToCompleteTheTable) + "-" + year);
//        System.out.println(month);
        for (final int[] i = {0}; i[0] < countColumn; i[0]++) {
            TableColumn oneDay = new TableColumn(changeColumnText(i[0] + 1, month, year));
//            oneDay.setCellValueFactory(new PropertyValueFactory<PracownikFX, JFXButton>("addWorkTime"));
            oneDay.setMinWidth(150);
            oneDay.setCellFactory(tc -> {
                TableCell<PracownikFX, String> cell = new TableCell<PracownikFX, String>() {
                    @Override
                    protected void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);
//                        setText("X");
                        for (WorkTime w : workTimesForDate) {
                            String s[] = w.getData().split("-");
                            String header[] = getTableColumn().getText().split(" ");
                            if (s[0].equals(header[0]) && getIndex() < scheduler_tableView.getItems().size() && getIndex() > -1) {
                                if (Integer.toString(w.getPRA_ID_fk()).equals(scheduler_tableView.getItems().get(getIndex()).getId())) {
                                    setText(w.getGodzina_rozpoczecia() + "-" + w.getGodzina_zakonczenia() + "\n" + w.getFunkcja());
                                    commitEdit(w.getGodzina_rozpoczecia() + "-" + w.getGodzina_zakonczenia() + "\n" + w.getFunkcja());
                                    System.out.println("wstawiono cell");
                                    System.out.println(getText());
                                    break;
                                }
                            }
                        }
                        String header[] = getTableColumn().getText().split(" ");
                        if (header[1].equals("sobota") || header[1].equals("niedziela")) {
                            this.setStyle("-fx-background-color: #DDDDDD;" +
                                    "-fx-border-color: #CCCCCC;");
                        }
//                        setText(empty ? null : item);
                    }
                };

                cell.setOnMouseClicked(e -> {
//                    searchElementToComplete();
//                    System.out.println(cell.getTableRow().getIndex());
//                    System.out.println("Pracownik: " + scheduler_tableView.getItems().get(cell.getTableRow().getIndex()).getImie_nazwisko());
//                    System.out.println("W dniu: " + cell.getTableColumn().getText());
//                    System.out.println("cell index: " + cell.getIndex());
//
//                    System.out.println("--------------------------------");

                    AddWorkTimeController.setWorker_name_str(scheduler_tableView.getItems().get(cell.getTableRow().getIndex()).getImie_nazwisko());
                    AddWorkTimeController.setWorker_id(Integer.parseInt(scheduler_tableView.getItems().get(cell.getTableRow().getIndex()).getId()));

                    String[] day = cell.getTableColumn().getText().split(" ");
                    checkWorkTimeWorker(cell, Integer.parseInt(day[0]), ParseMonthPolishStringToNumber.changeMonthToInt(monthLabel.getText()) + 1, year);
                });
//                cell.setText(scheduler_tableView.getItems().get(cell.getTableRow().getIndex()).getId());
                return cell;
            });
            scheduler_tableView.getColumns().add(oneDay);

        }
//        for(int i=0; i<3; i++){
//            TableColumn oneDay = new TableColumn("");
//            scheduler_tableView.getColumns().add(oneDay);
//        }
        scheduler_tableView.setItems(data);
        scheduler_tableView.getSortOrder().add(workers_column);
    }

    private void checkWorkTimeWorker(TableCell<PracownikFX, String> cell, int day, int month, int year) {
        LocalTime time_from;
        LocalTime time_to;
        String functionInWork = "";
        if (cell.getText()==null) {
            time_from = LocalTime.of(12, 0, 0);
            time_to = LocalTime.of(20, 0, 0);
            functionInWork = "Restauracja";
            showAddWorkTime(day, month, year);
        } else {
            URL url = getClass().getClassLoader().getResource("sample/Layouts/MenuEditDelete.fxml");
            FXMLLoader loader = new FXMLLoader(url);
            MenuEditDeleteController controller = loader.getController();

            Scene scene = null;
            try {
                scene = new Scene(loader.load());
            } catch (IOException e) {
                e.printStackTrace();
            }

            Stage primaryStage = (Stage) scheduler_tableView.getScene().getWindow();
            Stage secondaryStage = new Stage();
            secondaryStage.setTitle("Chochołowy Dwór");
            secondaryStage.setScene(scene);

            secondaryStage.initModality(Modality.WINDOW_MODAL);
            secondaryStage.initOwner(primaryStage);
            secondaryStage.initStyle(StageStyle.UNDECORATED);
            secondaryStage.show();
            menuEditDeleteStage=secondaryStage;

            //parse text from cell

        }
    }

    public static void closeStage(){
        menuEditDeleteStage.close();
    }

    private void showAddWorkTime(int day, int month, int year) {
        AddWorkTimeController.setController(this);
        URL url = getClass().getClassLoader().getResource("sample/Layouts/AddWorkTime.fxml");
        FXMLLoader loader = new FXMLLoader(url);

        Scene scene = null;
        try {
            scene = new Scene(loader.load());
        } catch (IOException e) {
            e.printStackTrace();
        }

        Stage primaryStage = (Stage) scheduler_tableView.getScene().getWindow();
        Stage secondaryStage = new Stage();
        secondaryStage.setTitle("Chochołowy Dwór");
        secondaryStage.setScene(scene);
//        System.out.println(day + " " + month+ " " + year);
        AddWorkTimeController controller = loader.getController();
        //controller.set_
        String day_str;
        if (day < 10) {
            day_str = "0" + day;
        } else {
            day_str = Integer.toString(day);
        }
        String month_str;
        if (month < 10) {
            month_str = "0" + month;
        } else {
            month_str = Integer.toString(month);
        }
        String year_str = Integer.toString(year);
        controller.setDate(day_str, month_str, year_str);


        secondaryStage.initModality(Modality.WINDOW_MODAL);
        secondaryStage.initOwner(primaryStage);
        secondaryStage.show();
    }

//    public  void searchElementToComplete(){
//
//        for (WorkTime w : workTimesForDate) {
//
//            for(TableCell<PracownikFX,String> cell: cells) {
////                System.out.println(cell.getIndex());
////                cell.setText("test");
//                String s[] = w.getData().split("-");
//                String header[] = cell.getTableColumn().getText().split(" ");
//                if (s[0].equals(header[0])&& cell.getIndex()<scheduler_tableView.getItems().size() && cell.getIndex()>-1 ) {
//                        if(Integer.toString(w.getPRA_ID_fk()).equals(scheduler_tableView.getItems().get(cell.getIndex()).getId())){
//                            cell.setText(w.getGodzina_rozpoczecia() + "-" + w.getGodzina_zakonczenia() + "  :  " + w.getFunkcja());
//                            cell.commitEdit(w.getGodzina_rozpoczecia() + "-" + w.getGodzina_zakonczenia() + "  :  " + w.getFunkcja());
//                            System.out.println("wstawiono");
//                            System.out.println(cell.getText());
//                            break;
//                        }
//                }
//            }
////            cell.setText(cell.getTableColumn().getText());
////            cell.setText(scheduler_tableView.getItems().get((cell.getIndex()==-1 ? 0 : cell.getIndex())).getId());
//        }
////
//    }

    private void completeTheTable(String date) {
        List<WorkTime> workTimes = OperationsDBTime.getWorkTimeWithDate(date);
        workTimesForDate.clear();
        for (WorkTime w : workTimes) {
//            System.out.println("pobrane z bazy: "+w.getGodzina_rozpoczecia()+"-"+w.getGodzina_zakonczenia());
            String s[] = w.getData().split("-");
            String dateGet = s[1] + "-" + s[2];
            if (dateGet.equals(date)) {
                workTimesForDate.add(w);
                System.out.println(w.getGodzina_rozpoczecia() + "-" + w.getGodzina_zakonczenia());
            }
        }
        System.out.println("------------------------------");

//        System.out.println(scheduler_tableView.getColumns().get(2).getCellData(1));
    }

    private void showDate() {
        Calendar calendar = Calendar.getInstance();
        int month = calendar.get(Calendar.MONTH);
        int year = calendar.get(Calendar.YEAR);
        monthLabel.setText(ParseMonthToPolishString.changeMonthToString(month));
        yearLabel.setText(Integer.toString(year));
        countColumn = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
        createTable(month, year);
    }






    public void setNextMonth() {
        int month = ParseMonthPolishStringToNumber.changeMonthToInt(monthLabel.getText()) + 1;
        int year = Integer.parseInt(yearLabel.getText());
        if (month > 11) {
            year++;
            month = 0;
        }
        createTable(month, year);
//        changeColumnText(month,year);
        monthLabel.setText(ParseMonthToPolishString.changeMonthToString(month));
        yearLabel.setText(Integer.toString(year));
    }

    public void setPrevMonth() {
        int month = ParseMonthPolishStringToNumber.changeMonthToInt(monthLabel.getText()) - 1;
        int year = Integer.parseInt(yearLabel.getText());
        if (month < 0) {
            year--;
            month = 11;
        }
        createTable(month, year);
//        changeColumnText(month,year);
        monthLabel.setText(ParseMonthToPolishString.changeMonthToString(month));
        yearLabel.setText(Integer.toString(year));
    }

//    private void clearTable(){
//        for(TableCell<PracownikFX,String> cell: cells) {
//            cell.setText("");
//        }
//    }

    private String changeColumnText(int day, int month, int year) {

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.MONTH, month);
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.DAY_OF_MONTH, day - 1);

        int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);

        String columnText = "";

        switch (dayOfWeek) {
            case 1:
                columnText = (day < 10 ? "0" + day : Integer.toString(day)) + " " + "poniedziałek";
                break;

            case 2:
                columnText = (day < 10 ? "0" + day : Integer.toString(day)) + " " + "wtorek";
                break;

            case 3:
                columnText = (day < 10 ? "0" + day : Integer.toString(day)) + " " + "środa";
                break;

            case 4:
                columnText = (day < 10 ? "0" + day : Integer.toString(day)) + " " + "czwartek";
                break;

            case 5:
                columnText = (day < 10 ? "0" + day : Integer.toString(day)) + " " + "piątek";
                break;

            case 6:
                columnText = (day < 10 ? "0" + day : Integer.toString(day)) + " " + "sobota";
                break;

            case 7:
                columnText = (day < 10 ? "0" + day : Integer.toString(day)) + " " + "niedziela";
                break;
        }


        return columnText;
    }


    @Override
    public void start(Stage primaryStage) throws Exception {

    }
}
