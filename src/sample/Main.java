package sample;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import sample.Database.OperacjeDBPracownik;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("Layouts/Login.fxml"));
        primaryStage.setTitle("Chochołowy Dwór");
        primaryStage.setScene(new Scene(root));
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
//        OperacjeDBPracownik operacjeDBPracownik = new OperacjeDBPracownik();
//        operacjeDBPracownik.dodajPracownika(1,"Kacper", "Seweryn","capsel","test123");
    }
}
