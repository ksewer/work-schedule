package sample.Database;

import javax.persistence.*;
import java.util.List;

public class OperationsDBTime {

    private static EntityManagerFactory ENTITY_MANAGER_FACTORY = Persistence
            .createEntityManagerFactory("grafik_chd");

    public static WorkTime addWorkTime(String date, String time_from, String time_to, String function, int count_time, int PRA_ID_fk ){
        EntityManager em = ENTITY_MANAGER_FACTORY.createEntityManager();
        EntityTransaction et = null;
        WorkTime workTime=null;
        try{
            et = em.getTransaction();
            et.begin();
            workTime = new WorkTime();
            workTime.setData(date);
            workTime.setGodzina_rozpoczecia(time_from);
            workTime.setGodzina_zakonczenia(time_to);
            workTime.setFunkcja(function);
            workTime.setIl_minut(count_time);
            workTime.setPRA_ID_fk(PRA_ID_fk);
            em.persist(workTime);
            et.commit();

        }catch (Exception e){
            if(et != null){
                et.rollback();
            }
            e.printStackTrace();
        }
        finally {
            em.close();
        }
        return workTime;
    }


    public static List<WorkTime> getWorkTimes() {
        EntityManager em = ENTITY_MANAGER_FACTORY.createEntityManager();
        String query = "SELECT w FROM WorkTime w WHERE w.id IS NOT NULL";
        TypedQuery<WorkTime> tq = em.createQuery(query, WorkTime.class);
        List<WorkTime> workTimes = null;
        try{
            workTimes = tq.getResultList();

        }catch(NoResultException e){
            System.out.println("Brak uzytnkownikow w bazie.");
        }finally {
            em.close();
            return workTimes;
        }
    }

    public static List<WorkTime> getWorkTimeWithDate(String date) {
        EntityManager em = ENTITY_MANAGER_FACTORY.createEntityManager();
        TypedQuery<WorkTime> tq = em.createQuery(" select w FROM WorkTime w WHERE w.data LIKE :date",WorkTime.class);
        tq.setParameter("date", "%"+date+"%");
        List<WorkTime> workTimes = null;
        try{
            workTimes = tq.getResultList();

        }catch(NoResultException e){
            System.out.println("Brak uzytnkownikow w bazie.");
        }finally {
            em.close();
            return workTimes;
        }
    }
}
