package sample.Database;

import java.io.Serializable;
import javax.persistence.*;

@Entity
@Table(name = "pracownik")
public class Pracownik implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "PRA_ID", unique = true)
    private int id;

    @Column(name = "PRA_IMIE", nullable = false)
    private String imie;

    @Column(name = "PRA_NAZWISKO", nullable = false)
    private String nazwisko;

    @Column(name = "PRA_STANOWISKO", nullable = false)
    private String stanowisko;

    @Column(name = "PRA_LOGIN", nullable = false)
    private String login;

    @Column(name = "PRA_HASLO", nullable = false)
    private String haslo;

    public static int hoursWorking;

    public Pracownik(String imie, String nazwisko, String stanowisko, String login, String haslo) {
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.stanowisko = stanowisko;
        this.login = login;
        this.haslo = haslo;
    }

    public Pracownik() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getImie() {
        return imie;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    public String getNazwisko() {
        return nazwisko;
    }

    public void setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }

    public String getStanowisko() {
        return stanowisko;
    }

    public void setStanowisko(String stanowisko) {
        this.stanowisko = stanowisko;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getHaslo() {
        return haslo;
    }

    public void setHaslo(String haslo) {
        this.haslo = haslo;
    }
}
