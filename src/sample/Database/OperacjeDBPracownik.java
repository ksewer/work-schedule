package sample.Database;

import javax.persistence.*;
import java.util.List;

public class OperacjeDBPracownik {

    private static EntityManagerFactory ENTITY_MANAGER_FACTORY = Persistence
            .createEntityManagerFactory("grafik_chd");

    public static void dodajPracownika(String imie, String nazwisko, String stanowisko, String login, String haslo){
        EntityManager em = ENTITY_MANAGER_FACTORY.createEntityManager();
        EntityTransaction et = null;
        try{
            et = em.getTransaction();
            et.begin();
            Pracownik pracownik = new Pracownik();
            pracownik.setImie(imie);
            pracownik.setNazwisko(nazwisko);
            pracownik.setStanowisko(stanowisko);
            pracownik.setLogin(login);
            pracownik.setHaslo(haslo);
            em.persist(pracownik);
            et.commit();
        }catch (Exception e){
            if(et != null){
                et.rollback();
            }
            e.printStackTrace();
        }
        finally {
            em.close();
        }
    }

    public static Pracownik getPracownik(int id) {
        EntityManager em = ENTITY_MANAGER_FACTORY.createEntityManager();
        String query = "SELECT p FROM Pracownik p WHERE p.id = :PRA_ID";

        TypedQuery<Pracownik> tq = em.createQuery(query, Pracownik.class);
        tq.setParameter("PRA_ID", id);
        Pracownik pracownik = null;
        try{
            pracownik = tq.getSingleResult();
        }catch(NoResultException e){
            System.out.println("Nie znaleziono uzytkownika o podanym id.");
        }
        finally {
            em.close();
            return pracownik;
        }
    }

    public static Pracownik getPracownik(String login) {
        EntityManager em = ENTITY_MANAGER_FACTORY.createEntityManager();
        String query = "SELECT p FROM Pracownik p WHERE p.login = :PRA_LOGIN";

        TypedQuery<Pracownik> tq = em.createQuery(query, Pracownik.class);
        tq.setParameter("PRA_LOGIN", login);
        Pracownik pracownik = null;
        try{
            pracownik = tq.getSingleResult();
        }catch(NoResultException e){
            System.out.println("Uzytkownik o podanym loginie nie istnieje.");
        }
        finally {
            em.close();
            return pracownik;
        }
    }


    public static List<Pracownik> getPracownicy() {
        EntityManager em = ENTITY_MANAGER_FACTORY.createEntityManager();
        String query = "SELECT p FROM Pracownik p WHERE p.id IS NOT NULL";
        TypedQuery<Pracownik> tq = em.createQuery(query, Pracownik.class);
        List<Pracownik> pracownicy = null;
        try{
            pracownicy = tq.getResultList();

        }catch(NoResultException e){
            System.out.println("Brak uzytnkownikow w bazie.");
        }finally {
            em.close();
            return pracownicy;
        }
    }

    public static void edytujPracownika(int id, String imie, String nazwisko, String stanowisko, String login, String haslo) {
        EntityManager em = ENTITY_MANAGER_FACTORY.createEntityManager();
        EntityTransaction et = null;
        Pracownik pracownik = null;
        try{
            et = em.getTransaction();
            et.begin();
            pracownik = em.find(Pracownik.class, id);
            pracownik.setImie(imie);
            pracownik.setNazwisko(nazwisko);
            pracownik.setStanowisko(stanowisko);
            pracownik.setLogin(login);
            pracownik.setHaslo(haslo);
            em.persist(pracownik);
            et.commit();
        }catch (Exception e){
            if(et != null){
                et.rollback();
            }
            e.printStackTrace();
        }
        finally {
            em.close();
        }
    }

    public static void usunPracownika(int id) {
        EntityManager em = ENTITY_MANAGER_FACTORY.createEntityManager();
        EntityTransaction et = null;
        Pracownik pracownik = null;
        try{
            et = em.getTransaction();
            et.begin();
            pracownik = em.find(Pracownik.class, id);
            em.remove(pracownik);
            et.commit();
        }catch (Exception e){
            if(et != null){
                et.rollback();
            }
            e.printStackTrace();
        }
        finally {
            em.close();
        }
    }

}
