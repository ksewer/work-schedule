package sample.Database;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "czas_pracy")
public class WorkTime implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "CZA_ID", unique = true)
    private int id;

    @Column(name = "CZA_DATA", nullable = false)
    private String data;

    @Column(name = "CZA_GODZINA_ROZP", nullable = false)
    private String godzina_rozpoczecia;

    @Column(name = "CZA_GODZINA_ZAK", nullable = false)
    private String godzina_zakonczenia;

    @Column(name = "CZA_FUNKCJA", nullable = false)
    private String funkcja;

    @Column(name = "CZA_IL_MINUT", nullable = false)
    private int il_minut;

    @Column(name = "PRA_ID_fk", nullable = false)
    private int PRA_ID_fk;

    public WorkTime(){

    }

    public WorkTime(String data, String godzina_rozpoczecia, String godzina_zakonczenia, String funkcja, int PRA_ID_fk) {
        this.data = data;
        this.godzina_rozpoczecia = godzina_rozpoczecia;
        this.godzina_zakonczenia = godzina_zakonczenia;
        this.funkcja = funkcja;
        this.PRA_ID_fk = PRA_ID_fk;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public int getId() {
        return id;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getGodzina_rozpoczecia() {
        return godzina_rozpoczecia;
    }

    public void setGodzina_rozpoczecia(String godzina_rozpoczecia) {
        this.godzina_rozpoczecia = godzina_rozpoczecia;
    }

    public String getGodzina_zakonczenia() {
        return godzina_zakonczenia;
    }

    public void setGodzina_zakonczenia(String godzina_zakonczenia) {
        this.godzina_zakonczenia = godzina_zakonczenia;
    }

    public String getFunkcja() {
        return funkcja;
    }

    public void setFunkcja(String funkcja) {
        this.funkcja = funkcja;
    }

    public int getIl_minut() {
        return il_minut;
    }

    public void setIl_minut(int il_minut) {
        this.il_minut = il_minut;
    }

    public int getPRA_ID_fk() {
        return PRA_ID_fk;
    }

    public void setPRA_ID_fk(int PRA_ID_fk) {
        this.PRA_ID_fk = PRA_ID_fk;
    }
}
