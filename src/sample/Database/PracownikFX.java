package sample.Database;

import com.jfoenix.controls.JFXButton;
import javafx.beans.property.SimpleStringProperty;

public class PracownikFX {

    private final SimpleStringProperty id;
    private final SimpleStringProperty imie;
    private final SimpleStringProperty nazwisko;
    private final SimpleStringProperty stanowisko;
    private final SimpleStringProperty login;
    private final SimpleStringProperty imie_nazwisko;
    private JFXButton addWorkTime;


    public PracownikFX(String id, String imie, String nazwisko, String stanowisko, String login) {
        this.id = new SimpleStringProperty(id);
        this.imie = new SimpleStringProperty(imie);
        this.nazwisko = new SimpleStringProperty(nazwisko);
        this.stanowisko = new SimpleStringProperty(stanowisko);
        this.login = new SimpleStringProperty(login);
        this.imie_nazwisko = new SimpleStringProperty(imie+" "+nazwisko);
        this.addWorkTime = new JFXButton("Add");
    }

    public String getId() {
        return id.get();
    }

    public SimpleStringProperty idProperty() {
        return id;
    }

    public void setId(String id) {
        this.id.set(id);
    }

    public String getImie() {
        return imie.get();
    }

    public SimpleStringProperty imieProperty() {
        return imie;
    }

    public void setImie(String imie) {
        this.imie.set(imie);
    }

    public String getNazwisko() {
        return nazwisko.get();
    }

    public SimpleStringProperty nazwiskoProperty() {
        return nazwisko;
    }

    public void setNazwisko(String nazwisko) {
        this.nazwisko.set(nazwisko);
    }

    public String getStanowisko() {
        return stanowisko.get();
    }

    public SimpleStringProperty stanowiskoProperty() {
        return stanowisko;
    }

    public void setStanowisko(String stanowisko) {
        this.stanowisko.set(stanowisko);
    }

    public String getLogin() {
        return login.get();
    }

    public SimpleStringProperty loginProperty() {
        return login;
    }

    public void setLogin(String login) {
        this.login.set(login);
    }

    public String getImie_nazwisko() {
        return imie_nazwisko.get();
    }

    public SimpleStringProperty imie_nazwiskoProperty() {
        return imie_nazwisko;
    }

    public void setImie_nazwisko(String imie_nazwisko) {
        this.imie_nazwisko.set(imie_nazwisko);
    }

    public JFXButton getAddWorkTime() {
        return addWorkTime;
    }

    public void setAddWorkTime(JFXButton addWorkTime) {
        this.addWorkTime = addWorkTime;
    }
}
