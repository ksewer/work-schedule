package sample.Utilities;

public class ParseMonthToPolishString {
    public static String changeMonthToString(int month) {
        String monthS = "";
        switch (month) {
            case 0:
                monthS = "styczeń";
                break;

            case 1:
                monthS = "luty";
                break;

            case 2:
                monthS = "marzec";
                break;

            case 3:
                monthS = "kwiecień";
                break;

            case 4:
                monthS = "maj";
                break;

            case 5:
                monthS = "czerwiec";
                break;

            case 6:
                monthS = "lipiec";
                break;

            case 7:
                monthS = "sierpień";
                break;

            case 8:
                monthS = "wrzesień";
                break;

            case 9:
                monthS = "październik";
                break;

            case 10:
                monthS = "listopad";
                break;

            case 11:
                monthS = "grudzień";
                break;
        }

        return monthS;
    }
}
