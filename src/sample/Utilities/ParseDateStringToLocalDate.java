package sample.Utilities;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class ParseDateStringToLocalDate {

    public static final LocalDate parse_date (String dateString){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        LocalDate localDate = LocalDate.parse(dateString, formatter);
        return localDate;
    }

}
