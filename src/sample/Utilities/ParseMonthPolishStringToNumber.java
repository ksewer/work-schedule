package sample.Utilities;

public class ParseMonthPolishStringToNumber {

    public static int changeMonthToInt(String monthS) {
        int month = -1;
        switch (monthS) {
            case "styczeń":
                month = 0;
                break;

            case "luty":
                month = 1;
                break;

            case "marzec":
                month = 2;
                break;

            case "kwiecień":
                month = 3;
                break;

            case "maj":
                month = 4;
                break;

            case "czerwiec":
                month = 5;
                break;

            case "lipiec":
                month = 6;
                break;

            case "sierpień":
                month = 7;
                break;

            case "wrzesień":
                month = 8;
                break;

            case "październik":
                month = 9;
                break;

            case "listopad":
                month = 10;
                break;

            case "grudzień":
                month = 11;
                break;
        }

        return month;
    }
}
