package sample.toolbar;

import com.jfoenix.controls.JFXButton;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.stage.Stage;
import sample.Controllers.GrafikController;

import java.io.IOException;
import java.net.URL;
import java.util.Calendar;
import java.util.ResourceBundle;

public class ToolbarController implements Initializable {

    @FXML
    JFXButton usr_btn;
    @FXML
    JFXButton gra_btn;
    @FXML
    JFXButton res_btn;
    @FXML
    JFXButton sett_btn;



    @Override
    public void initialize(URL location, ResourceBundle resources) {



    }

    public void showUsers(){
        URL url = getClass().getClassLoader().getResource("sample/Layouts/Users.fxml");
        changeScene(url);
    }

    public void showReservations(){
        URL url = getClass().getClassLoader().getResource("sample/Layouts/Reservations.fxml");
        changeScene(url);
    }

    public void showScheduler(){
        URL url = getClass().getClassLoader().getResource("sample/Layouts/Scheduler.fxml");
        changeScene(url);
    }

    public void showSettings(){
        URL url = getClass().getClassLoader().getResource("sample/Layouts/Settings.fxml");
        changeScene(url);
    }

    private void changeScene(URL url){
        FXMLLoader loader = new FXMLLoader(url);
        Stage stage = (Stage) usr_btn.getScene().getWindow();
        Scene scene = null;
        try {
            scene = new Scene(loader.load());
        } catch (IOException e) {
            e.printStackTrace();
        }
        stage.setScene(scene);
//        URL urlCheck = getClass().getClassLoader().getResource("sample/Layouts/Scheduler.fxml");
//        if(url.equals(urlCheck)){
//            GrafikController controller = loader.getController();
//            controller.searchElementToComplete();
//        }
    }
}
